# Dummy Django project for future experiments

This is a completely dummy django app created from <https://github.com/jefftriplett/django-startproject> with a simple todo application.
It's only purpose is to be used as an example app in my other projects.

This is **NOT production-ready** (e.g. DB is configured with "POSTGRES_HOST_AUTH_METHOD=trust" setting to allow anyone to access).
