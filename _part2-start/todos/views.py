from django.shortcuts import render, get_object_or_404, redirect
from django.views import generic
from .models import Task


class IndexView(generic.ListView):
    template_name = "todos/index.html"
    context_object_name = "all_tasks"

    def get_queryset(self):
        return Task.objects.all()


class DetailView(generic.DetailView):
    model = Task
    template_name = "todos/detail.html"


def add(request):
    title = request.POST["title"]
    task = Task(title=title)
    task.save()
    return redirect("todos:index")
